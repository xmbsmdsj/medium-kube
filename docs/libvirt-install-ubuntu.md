# Virtualization on Ubuntu

```sh
# Install qemu/kvm package
$ sudo apt install qemu-kvm
```


```sh
# Install libvirt daemon
$ sudo apt install libvirt-daemon-system
```

```sh
# Install libvirt dev library
$ sudo apt install libvirt-dev
```

TODO: Install containerd