# Hybrid-Orchestration

VMs and containers are both powerful tools in their area. VMs are heavyweight and are capable of doing complicated tasks whereas containers are lightweight, which is great for scaling applications out elastically. These two things are so different that they cannot simply replace each other, that's where hybrid-orchestration comes in.

# Hybrid orchestration in mediumkube

Before we have hybrid orchestration, we start processes listening on gateway to provide services such like proxies to mediumkube domains. The bad news is that processes are not managed by mediumkube easily.

Now, we propose a component called extension, which extends the ability of domains (VMs). Extensions are stateless containers, deployed locally and shared by all domains on same host machine, like this.

![](./ext/ext-set.drawio.png)

To configure extensions, simple add this to configuration file

```yaml
extensions:
  - name: echo
    image: docker.io/wylswz/echo:latest
    port: 4567
    args: ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "4567"]

```


This configuration adds an extension called `echo` to extension set, listening on port `4567` of the container. Mediumkube attaches network to extensions using `bridge` and `dhcp` CNI plugins.

In addition, Mediumkube does port mapping for the extension, therefore, users/domains are able to access extensions on bridge without knowing their ip addresses. In this case

```
makaveli@makaveli-PC:~/Desktop/mediumkube$ sudo ./build/mediumkube ext list
2022/03/17 19:43:26 Using configuration file:  /etc/mediumkube/config.yaml
I0317 19:43:26.591343  278268 extension.go:64] Labels: map[cri-ip-addr:10.114.114.134]
+------+------------------------------+----------------+------+---------+
| NAME |            IMAGE             |       IP       | PORT | MESSAGE |
+------+------------------------------+----------------+------+---------+
| echo | docker.io/wylswz/echo:latest | 10.114.114.134 | 4567 |         |
+------+------------------------------+----------------+------+---------+

```

you can call extension using either
```sh
curl 10.114.114.134:4567
```

or 

```sh
curl 10.114.114.1:4567
```

The good thing about this is that the domains always know how to access extension, because the `10.114.114.1` gateway is rendered by cloud init, and extension ports are also defined in configuration files. Otherwise, additional service discoveries are required. 

# Configuring containerd

Extensions are powered by containerd, therefore additional configurations are required in order to access docker registry


This configuration file should be placed under `/etc/containerd/config.toml`
```toml
version = 2

[plugins."io.containerd.grpc.v1.cri".registry]
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
      endpoint = ["https://registry-1.docker.io"]
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."gcr.io"]
      endpoint = ["https://gcr.io"]
  [plugins."io.containerd.grpc.v1.cri".registry.configs]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."docker.io".auth]
      username = {username}
      password = {password}
```

# External resources

[虚拟机与容器的混合管理实践](https://segmentfault.com/a/1190000040926384)

[虚拟机和容器超融合试验研究](./paper/虚拟机和容器超融合试验研究.pdf)
