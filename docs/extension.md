# Mediumkube Extension


## Install containerd

## Network namespace configuration

Currently, extensions share one network namespace `ns-mediumkube`. You'll need to create network namespace by yourself

```sh
$ sudo ip netns add ns-mediumkube

```

## Start dhcp

```sh
$ ./hack/dhcp.sh
```

## Containerd configuration

[configure image registry](https://github.com/containerd/containerd/blob/main/docs/cri/registry.md)