from fastapi import FastAPI

app = FastAPI()

@app.get("/foo")
def foo():
    return "bar"
