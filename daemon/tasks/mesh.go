package tasks

var ()

// StartMesh is invoked repeatdly, so makesure everything inside this method
// is idempotent
func StartMesh() {
	ConfigFlannel()
	InitETCDDnsDir()
	CommerceSync()
}

func init() {
}
