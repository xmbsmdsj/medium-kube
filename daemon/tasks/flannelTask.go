package tasks

import (
	"mediumkube/pkg/configurations"
	"mediumkube/pkg/procd"
	"mediumkube/pkg/utils"
	"os"

	"github.com/vishvananda/netlink"
)

var (
	route *netlink.Route
)

const (
	ipv4                  int    = netlink.FAMILY_V4
	flannelExecutableName string = "mediumkube-flanneld"
)

func NewFlannelTask() *procd.DaemonCmdTask {
	etcdPort := configurations.Config().Overlay.EtcdPort
	master := configurations.Config().Overlay.Master
	t := procd.NewDaemonCmdTask(
		flannelExecutableName,
		"--etcd-endpoints", utils.EtcdEp(master, etcdPort),
		"--etcd-prefix", configurations.Config().Overlay.Flannel.EtcdPrefix,
		"--ip-masq",
	)
	t.AttachWriterToStdout(os.Stdout)
	return t
}
