package tasks

import (
	"context"
	"fmt"
	"mediumkube/pkg/common/flannel"
	"mediumkube/pkg/configurations"
	"mediumkube/pkg/procd"
	"mediumkube/pkg/utils"
	"os"
	"strings"

	etcd "mediumkube/pkg/etcd"

	clientv2 "go.etcd.io/etcd/client/v2"

	"k8s.io/klog/v2"
)

const (
	etcdExecutable = "mediumkube-etcd"
)

func NewEtcdTask() *procd.DaemonCmdTask {
	etcdPort := configurations.Config().Overlay.EtcdPort
	master := configurations.Config().Overlay.Master
	t := procd.NewDaemonCmdTask(
		etcdExecutable,
		fmt.Sprintf("--listen-client-urls=%s", utils.EtcdEp("0.0.0.0", etcdPort)),
		fmt.Sprintf("--advertise-client-urls=%s", utils.EtcdEp(master, etcdPort)),
		"--enable-v2=true",
	)
	t.AttachWriterToStdout(os.Stdout)
	return t
}

func InitETCDDnsDir() {
	overlayConfig := configurations.Config().Overlay
	_, err := etcd.ETCDV3Put(context.TODO(), overlayConfig.DNSEtcdPrefix, "")
	if err != nil && !strings.Contains(err.Error(), "already exists") {
		klog.Errorf("Error init dir: %s, err: %v", overlayConfig.DNSEtcdPrefix, err)
	}

	_, err = etcd.ETCDV3Put(context.TODO(), overlayConfig.LeaseEtcdPrefix, "")
	if err != nil && !strings.Contains(err.Error(), "already exists") {
		klog.Errorf("Error init dir: %s, err: %v", overlayConfig.LeaseEtcdPrefix, err)
	}
}

// Flannel uses v2
// configFlannel render flannel configuration fron overall configurations
// and push to etcd
func ConfigFlannel() {
	overlayConfig := configurations.Config().Overlay
	cli := etcd.NewClientOrDie()
	k := strings.Join([]string{overlayConfig.Flannel.EtcdPrefix, "config"}, "/")
	v := flannel.NewConfig(configurations.Config()).ToStr()
	kpi := clientv2.NewKeysAPI(cli)
	_, err := kpi.Set(context.TODO(), k, v, &clientv2.SetOptions{})
	if err != nil && !strings.Contains(err.Error(), "already exists") {
		klog.Errorf("Error init dir: %s, err: %v", overlayConfig.LeaseEtcdPrefix, err)
	}

}
