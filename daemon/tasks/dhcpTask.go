package tasks

import (
	"mediumkube/pkg/procd"
	"os/exec"

	"k8s.io/klog/v2"
)

// sudo rm -rf /run/cni/dhcp.sock && sudo /usr/local/mediumkube/bin/dhcp daemon

func NewDHCPTask() *procd.DaemonCmdTask {
	t := *procd.NewDaemonCmdTask("/usr/local/mediumkube/bin/dhcp", "daemon")
	t.RegisterPreRun(func() {
		klog.Info("Removing existing dhcp sock")
		exec.Command("rm", "-rf", "/run/cni/dhcp.sock").Run()
	})
	return &t
}
