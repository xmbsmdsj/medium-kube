package cni

import (
	"mediumkube/pkg/common"
	"mediumkube/pkg/configurations"
)

type BridgeConfig struct {
	Name             string `json:"name"`
	CNIVersion       string `json:"cniVersion"`
	Type             string `json:"type"`
	Bridge           string `json:"bridge,omitempty"`
	IsGateway        bool   `json:"isGateway,omitempty"`        // Assign an IP address to the bridge. Defaults to false.
	IsDefaultGateway bool   `json:"isDefaultGateway,omitempty"` // Sets isGateway to true and makes the assigned IP the default route. Defaults to false.
	ForceAddress     bool   `json:"forceAddress,omitempty"`     // Indicates if a new IP address should be set if the previous value has been changed. Defaults to false.
	IPMasq           bool   `json:"ipMasq,omitempty"`           // set up IP Masquerade on the host for traffic originating from this network and destined outside of it. Defaults to false.
	MTU              bool   `json:"mtu,omitempty"`
	HairPinModel     bool   `json:"hairpinMode,omitempty"` // set hairpin mode for interfaces on the bridge. Defaults to false.
	PromiscMode      bool   `json:"promiscMode,omitempty"` // set promiscuous mode on the bridge. Defaults to false.
	IPAM             IPAM   `json:"ipam,omitempty"`        // IPAM configuration to be used for this network.
}

// https://www.cni.dev/plugins/v0.7/ipam/
type IPAM struct {
	Type   string `json:"type,omitempty"`   // dhcp or host-local
	Subnet string `json:"subnet,omitempty"` // cidr
}

func NewConfig(ext common.Extension) BridgeConfig {
	ipam := IPAM{Type: "dhcp"}
	return BridgeConfig{
		CNIVersion:       "1.0.0",
		Name:             ext.BridgeNetworkName(),
		Bridge:           configurations.Config().BridgeName(),
		Type:             "bridge",
		IsDefaultGateway: true,
		IPMasq:           true,
		HairPinModel:     true,
		IPAM:             ipam,
	}
}
