package services

import (
	"context"
	"encoding/json"
	"mediumkube/pkg/common"
	"mediumkube/pkg/configurations"
	"mediumkube/pkg/etcd"

	"k8s.io/klog/v2"
)

type MeshService struct {
	config *common.OverallConfig
}

func (m *MeshService) ListLeases() ([]common.PeerLease, error) {
	res := make([]common.PeerLease, 0)
	resp, err := etcd.ETCDV3Get(context.TODO(), m.config.Overlay.LeaseEtcdPrefix)
	if err != nil {
		klog.Error(err)
		return []common.PeerLease{}, err
	}

	for _, node := range resp {
		payload := common.PeerLease{}
		if len(node.Value) == 0 {
			continue
		}
		err = json.Unmarshal([]byte(node.Value), &payload)
		if err != nil {

			klog.Errorf("Fail to marshal payload: %v, err: %v", node.Value, err)
			continue
		}
		res = append(res, payload)
	}
	return res, nil
}

func (m *MeshService) ListDomains() ([]common.Domain, error) {
	res := make([]common.Domain, 0)
	prefix := m.config.Overlay.DomainEtcdPrefix
	resp, err := etcd.ETCDV3Get(context.TODO(), prefix)
	if err != nil {
		return res, nil
	}

	for _, n := range resp {
		ds := make([]common.Domain, 0)
		err = json.Unmarshal([]byte(n.Value), &ds)
		if err != nil {
			klog.Error(err)
		}
		res = append(res, ds...)
	}
	return res, nil
}

func init() {
	InitMeshService(MeshService{config: configurations.Config()})
}
