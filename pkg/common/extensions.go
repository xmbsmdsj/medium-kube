package common

type ExtensionVO struct {
	Name    string
	Image   string
	Port    string
	Message string
	IP      string
}
