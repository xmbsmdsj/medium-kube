package network

// MapPort ip:port to corresponding port on bridge
func MapPort(bridgeIP, IP string, port, bridgePort string) {

	InsertRuleIfNotExists(
		TableNat, "PREROUTING", IPModAPP,
		"-p", "tcp",
		"-d", bridgeIP,
		"--dport", bridgePort,
		"-j", "DNAT",
		"--to-destination", IP,
	)
	InsertRuleIfNotExists(
		TableNat, "POSTROUTING", IPModAPP,
		"-p", "tcp",
		"-d", bridgeIP,
		"--dport", bridgePort,
		"-j", "MASQUERADE",
	)

}

func RemoveMapping(bridgeIP, IP string, port, bridgePort string) {
	DeleteIfExists(
		TableNat, "PREROUTING", []string{
			"-p", "tcp",
			"-d", bridgeIP,
			"--dport", bridgePort,
			"-j", "DNAT",
			"--to-destination", IP,
		},
	)

	DeleteIfExists(
		TableNat, "POSTROUTING", []string{
			"-p", "tcp",
			"-d", bridgeIP,
			"--dport", bridgePort,
			"-j", "MASQUERADE",
		},
	)
}
