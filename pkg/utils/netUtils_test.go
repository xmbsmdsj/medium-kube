package utils

import (
	"log"
	"testing"

	"k8s.io/klog/v2"
)

func TestGen(t *testing.T) {
	m := GenerateMac()
	log.Println(m)
}

func TestCidrToIP(t *testing.T) {
	b := "10.114.114.1/24"
	ip := GetIPFromCidr(b)
	klog.Info(ip)
}
