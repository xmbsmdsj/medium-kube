package utils

func Contains[T comparable](slice []T, t T) bool {
	for _, v := range slice {
		if v == t {
			return true
		}
	}
	return false
}

func SliceEq[T comparable](s1 []T, s2 []T) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i, _ := range s1 {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

func ContainsSlice[T comparable](slice [][]T, t []T) bool {
	for _, v := range slice {
		if SliceEq(v, t) {
			return true
		}
	}
	return false
}
