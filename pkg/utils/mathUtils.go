package utils

type Number interface {
	int | int16 | int32 | int64 | float32 | float64 | uint | uint16 | uint32 | uint64 | byte
}

func Max[T Number](i1 T, i2 T) T {

	if i1 > i2 {
		return i1
	}
	return i2
}
