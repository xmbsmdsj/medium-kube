package procd

import (
	"testing"
	"time"

	"k8s.io/klog/v2"
)

type task struct {
	val int64
	n   int64
}

func (t *task) Run() {
	t.n++
}

func abs(i int64) int64 {
	if i < 0 {
		return -i
	}
	return i
}
func calculateSkew(t *task, totalTime int64) int64 {

	return abs(t.n - totalTime/t.val)
}

func TestSchedule(t *testing.T) {
	tl := NewTaskList()
	klog.Info("Inited")
	for i := 10; i < 3000000; i += 100 {
		tl.Schedule(&task{val: int64(i)}, int64(i))
	}
	go tl.Start()
	klog.Info("Scheduled")

	time.Sleep(3 * time.Second)
	tl.Stop()

	skew := 0
	tl.forEach(func(n *Node) { skew += int(calculateSkew(n.task.(*task), 3000)) })
	klog.Infof("Skew: %v", skew)
}

func TestRunner(t *testing.T) {

}
