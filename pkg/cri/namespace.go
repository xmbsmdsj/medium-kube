package cri

import (
	"strings"

	"github.com/vishvananda/netns"
	"k8s.io/klog/v2"
)

const (
	linuxNsPrefix      = "/var/run/netns"
	linuxNsPrefixShort = "/run/netns"
)

func CreateNetworkNamespace(name string) error {
	name = strings.Replace(name, linuxNsPrefix, "", -1)
	name = strings.Replace(name, linuxNsPrefixShort, "", -1)
	_, err := netns.GetFromName(name)
	if err == nil {
		klog.Infof("Namespace %s exists, skip creatings", name)
		return nil
	}
	_, err = netns.NewNamed(name)
	if err != nil {
		klog.Error(err)
	}
	return err
}
