package dlock

import (
	"mediumkube/pkg/common"
)

type lockManager interface {
	DoWithLock(lockType string, timeout int64, f func(), fallback func())
}

type etcdLockManager struct {
	config *common.OverallConfig
}

func _lockKey(config *common.OverallConfig, lockType string) string {
	return config.Overlay.DLockEtcdPrefix + "/" + lockType
}

// DoWithLock
// ETCD based distributed lock
// 1. Try to get lock.
// 2. If key not exists, put myLock
// 	  Otherwise check TTL of existing lock. If it's timeout, then put myLock
// 3. Reget Lock and check UUID. If same, do job, otherwize, fallback
func (m *etcdLockManager) DoWithLock(lockType string, timeout int64, f func(), fallback func()) {
	f()
}

func NewEtcdLockManager(config *common.OverallConfig) *etcdLockManager {
	return &etcdLockManager{
		config: config,
	}
}

func init() {
	var _ lockManager = (*etcdLockManager)(nil)
}
